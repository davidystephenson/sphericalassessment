# sphericalassessment

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

To test the production build locally, open `dist/index.html` with your browser.
