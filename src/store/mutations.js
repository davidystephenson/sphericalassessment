const increase = (object, key) => object[key]
  ? object[key]++
  : (object[key] = 1)

const isIn = (property, value) => location => location[property]
  .toLowerCase()
  .indexOf(value.toLowerCase()) > -1

const refine = (property, value, locations) => value
  ? locations.filter(isIn(property, value))
  : locations

const count = (container, location) => {
  increase(container.ownership, location['Ownership Type'])

  location['Features - Products']
    .split(', ')
    .map(feature => {
      if (feature !== '') {
        increase(container.features, feature)
      }
    })
}

export const failure = (state, error) => {
  state.loading = false
  state.error = error.toString()
}

export const reset = (state, parameter) => {
  if (parameter) {
    state.filters[parameter] = ''
  } else {
    state.filters = {
      query: '',
      ownership: '',
      feature: ''
    }
  }
}

export const search = (state, filters) => {
  if (filters) {
    state.filters = filters
  }

  state.filtered.locations = refine(
    'Name', state.filters.query, state.locations
  )
  state.filtered.locations = refine(
    'Features - Products',
    state.filters.feature,
    state.filtered.locations
  )
  state.filtered.locations = refine(
    'Ownership Type',
    state.filters.ownership,
    state.filtered.locations
  )

  state.filtered.ownership = {}
  state.filtered.features = {}

  state.filtered.locations.map(location => count(state.filtered, location))
}

export const start = state => (state.loading = true)

export const success = (state, locations) => {
  state.error = false

  state.locations = locations
  state.filtered.locations = locations

  locations.map(location => count(state, location))

  state.filtered.ownership = state.ownership
  state.filtered.features = state.features

  state.loading = false
  state.loaded = true
}

export const toggle = state => (state.show = !state.show)
