export default {
  error: '',
  features: {},
  filters: {
    query: '',
    ownership: '',
    feature: ''
  },
  filtered: {
    locations: [],
    ownership: {},
    features: {}
  },
  locations: [],
  loading: false,
  loaded: false,
  ownership: {},
  show: false
}
