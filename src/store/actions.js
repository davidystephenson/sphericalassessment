export const getLocations = async ({ commit }) => {
  commit('start')

  const locations = require('./starbucks.json')

  commit('success', locations)
}

export const reset = ({ commit }, parameter) => {
  commit('reset', parameter)
  commit('search')
}
